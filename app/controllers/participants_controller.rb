require 'open-uri'
class ParticipantsController < ApplicationController
  before_filter :login_required , :except=>[:index,:show]
  layout :user_layout
  def index
     if !params[:sort].blank?
     sort = params[:sort]
     
     else
     sort = "event_id asc"  
    end
    if params[:event_id]
      
     @participants = Participant.where(:event_id=> params[:event_id]).paginate(:page=>params[:page],:per_page =>20).order(sort)
     @event_title = Event.find_by_id(params[:event_id]).name
    else
    @participants = Participant.paginate(:page=>params[:page],:per_page =>20).order(sort)
     end
  
  end

  def new
    
     @events = Event.select("id, name").where("name is not null")
      @states =NgState.select("id, state").where("state is not null")
    @participant = Participant.new
  end

  def create
   @uni_id = generateRandom
   
    if request.post?
      @participant = Participant.new params[:participant]
      @participant.uni_id = @uni_id if @participant.uni_id.blank?
      
      if @participant.save
        
        flash[:notice] = "Successfully Registered Participant!"
        
      else
        erro = ""
        
        
         @participant.errors.full_messages.each do |m|
           
          erro += m + "<br/>"
            
       
         end
        flash[:error] = "Unable to register Participant!<br/>" + erro
       
        @events = Event.select("id, name").where("name is not null")
      @states =NgState.select("id, state").where("state is not null")
        render :new
        
      end
      
     
    end
     redirect_to new_participant_url
      return false
  end

  def edit
    
    @participant = Participant.find(params[:id])
    @events = Event.select("id, name").where("name is not null")
    @states =NgState.select("id, state").where("state is not null")
    render :new
  
  end

  def update
    @participant  =  Participant.find(params[:id])
    if @participant.update_attributes(params[:participant])
       flash[:notice] = "Successfully Updated the Participant: #{params[:participant][:firstname]}"
       redirect_to new_participant_url
       return false
        
    else
      @participant = Participant.find(params[:id])
    @events = Event.select("id, name").where("name is not null")
    @states =NgState.select("id, state").where("state is not null")
    render :new
    end
       
  end

  def show
  end

  def destroy
    if Participant.find(params[:id]).destroy()
      flash[:notice] = "successfully deleted"
      
    else
      flash[:error] = "unable to delete!"
      
    end
    redirect_to participants_url
    return false
  end
  
  
  def export_to_excel
   if !params[:type]
     @clients =Participant.find(:all,:conditions => {"event_id" => params[:event_id]}) 
   end
   
  require 'spreadsheet'
  respond_to do |format|
      format.pdf
      format.xls {
        clients = Spreadsheet::Workbook.new
        list = clients.create_worksheet :name => 'All Participants'
        list.row(0).concat %w[Title Firstname  Lastname Othername Gender State LG]
        list.row(0).height = 30
        #list.column(0).width = 10
       # list.column(1).width = 20
       # list.column(2).width = 20
       # list.column(3).width = 20
       
         
        @clients.each_with_index { |client, i|
          list.row(i+1).push client.title ,client.firstname , client.lastname , client.othername, client.gender, client.ng_state.state, client.lgward, client.created_at.to_date
          
       
        }
        header_format = Spreadsheet::Format.new :color => :blue, :weight => :bold ,:align =>:center
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        clients.write blob
        #respond with blob object as a file
        send_data blob.string, :type => :xls, :filename => "Participants.xls"
      }
end
    
 end
  private
  
  def generateRandom
    
     r=Random.new
    
   # @uni_id =  r.rand(000100..999999)
   @uni_id =  r.rand((Time.now.nsec-1)..Time.now.nsec)
    uni_id = @uni_id.to_s
     
     st= "" 
     st += uni_id[r.rand(0..5)]
    uid = uni_id.chop.chop.chop
    
    if Participant.count(:conditions=>{:uni_id =>uid}) > 0
     
     generateRandom
     
   end
      return uid
      
      
    
    
    
  end
end
