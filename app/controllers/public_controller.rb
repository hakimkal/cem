class PublicController < ApplicationController
  
  def home
    @events = Event.where("name is not null and start_date >= ?", "#{Time.now.year}-#{Time.now.month}-#{Time.now.day}").limit(3)
    @event_images = EventImage.where("event_id is not null image_file_name is not null")
  end
  
  def about
    
 end
  
  def contact
    
  end
  
  def services
    
  end
end
