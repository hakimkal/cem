class NgStatesController < ApplicationController
  
   before_filter :login_required , :except=>[:index,:show]
   
   def add
    
    respond_to do |f|
      f.js
    end
  end
  
  def remove
    respond_to do |f|
      f.js
    end
  end
  layout :user_layout
  
  def new
    @ng_state = NgState.new
  end

   def create
    if request.post?
      speakers = params[:ng_state]
      params[:ng_state].each do |s|
        
        @event = NgState.new(s)
        @event.save
      end
    
    
    
    
    
    flash[:notice] = "Successfully saved new item"
    
    redirect_to ng_states_url and return
    
    
   end
  end
  def update
    
  end
  def destroy
    @partner= NgState.find(params[:id])
     
    if @partner.destroy
      
      flash[:notice] = "Succesfully deleted!"
    else
      flash[:error] = "Unable to delete, try later!"
    end
    redirect_to :back
  end
  def edit
  end

   

  def index
  end

  def show
  end

   
end
