class ApplicationController < ActionController::Base
  include SessionsHelper
  protect_from_forgery
  
  
  def login_required
     if signed_in?
      return true
    else
    flash[:warning]='Please login to continue...'
    session[:return_to]=request.url
    #redirect_to :controller => "users", :action => "login"
    redirect_to "/login"
    return false 
    end
  end
 
   
   
  def logout_required
    if signed_in?
      sign_out
    end
  end
  
  def check_staff?
    if current_user.group_id == 3
      flash.keep
      flash[:error] = "Restricted Section...Contact Admin!"
      redirect_to dashboard_path
    end
  end
  def user_layout
    
    if signed_in?
      return 'logged_in'
      
    else
      return 'application'
    end
  end
  
  def check_user_group
    
    if (signed_in? && (current_user.group.downcase == 'admin') || signed_in? && (current_user.group.downcase == 'manager'))
      return true
    else 
      flash[:error] = "You do not have sufficient privileges to access that area"
      redirect_to  request.env["HTTP_REFERER"]
    end
  end
end
