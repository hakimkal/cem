class UsersController < ApplicationController
  
  before_filter :login_required , :except=>[:index,:show]
  before_filter :check_staff?
  layout :user_layout
  

  def new
    @groups = Group.select("id, name").where("name is not null");
  
    @user = User.new
     
  end

  def create
    
    if request.post?
      
      @user  = User.new params[:user]
      
      if @user .save
        
        flash[:notice] = "Successfully created a new staff"
        redirect_to users_url
       return false
        
      else
        erro = ""
        
        
         @user .errors.full_messages.each do |m|
           
          erro += m + "<br/>"
            
       
         end
        flash[:error] = "Unable to create  staff<br/>" + erro
            
       render :new
        
      end
      
     
    end
     
    
    
  end

   

  def edit
    @groups = Group.select("id, name").where("name is not null");
  
    @user  =  User.find(params[:id])
     end
   

  
    
    def update
        @groups = Group.select("id, name").where("name is not null");
   # if params[:user][:password]  
    @user  =  User.find(params[:id])
    if @user.update_attributes(params[:user])
       flash[:notice] = "Successfully Updated the Staff: #{@user.firstname}"
       redirect_to user_url(params[:id])
       return false
        
    else 
     
   flash[:error] = "Unable to update #{@user.firstname}"
   redirect_to user_url(params[:id])
       return false
   
     end
     
  end

  def show
      @groups = Group.select("id, name").where("name is not null");
  
    @user  =  User.find(params[:id])
  
  end
   
   def change_my_password
     
     @user = User.find(current_user.id)
     
     
   end
  def destroy
    if User.find(params[:id]).destroy()
      flash[:notice] = "successfully deleted"
      
    else
      flash[:error] = "unable to delete!"
      
    end
    redirect_to users_url
    return false
  end

  def index
    if !params[:sort].blank?
       sort = params[:sort]
       
       else
       sort = "firstname asc"  
      end
     
    
    @users = User.paginate(:page=>params[:page],:per_page =>20).order(sort)
    
  end
end