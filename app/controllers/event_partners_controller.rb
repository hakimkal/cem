class EventPartnersController < ApplicationController
  
  before_filter :login_required , :except=>[:index,:show]
  layout :user_layout
  
  
  def add
    
    respond_to do |f|
      f.js
    end
  end
  
  def remove
    respond_to do |f|
      f.js
    end
  end
  def new
    @events = Event.select("id, name").where("name is not null")
    @event_partner = EventPartner.new
  end

  def edit
  end

  def create
    if request.post?
      speakers = params[:event_partner]
      params[:event_partner].each do |s|
        s[:event_id] = params[:event_images][:event_id]
        @event = EventPartner.new(s)
        @event.save
      end
    
    
    
    
    
    flash[:notice] = "Successfully saved new item"
    
    redirect_to event_url(params[:event_partner][0][:event_id]) and return
    
    
   end
  end
  def update
    
  end
  def destroy
    @partner= EventPartner.find(params[:id])
    @partner.logo = nil
    @partner.save
    if @partner.destroy
      
      flash[:notice] = "Succesfully deleted!"
    else
      flash[:error] = "Unable to delete, try later!"
    end
    redirect_to :back
  end
end
