class EventSpeakersController < ApplicationController
  before_filter :login_required , :except=>[:index,:show]
  layout :user_layout
  
  
  def add
    
    respond_to do |f|
      f.js
    end
  end
  
  def remove
    respond_to do |f|
      f.js
    end
  end
  def new
    @events = Event.select("id, name").where("name is not null")
    @event_speaker = EventSpeaker.new
  end

  def edit
     @events = Event.select("id, name").where("name is not null")
   
    @event_speaker = EventSpeaker.find(params[:id])
  end

  def create
    if request.post?
      speakers = params[:event_speaker]
      params[:event_speaker].each do |s|
        s[:event_id] = params[:event_images][:event_id]
        @event = EventSpeaker.new(s)
        @event.save
      end
    
    
    
    
    
    flash[:notice] = "Successfully saved new item"
    
    redirect_to event_url(params[:event_speaker][0][:event_id]) and return
    
    
   end
  end


  def update
    
    
    if request.put?
      @event_speaker = @model= EventSpeaker.find(:first,:conditions=>{:id =>params[:id]})
      if params[:event_speaker][:photo]
            
      @event_speaker.photo=nil
       @event_speaker.save
       
       end
       
      if @event_speaker.update_attributes(params[:event_speaker])
       flash[:notice] = "Successfully Updated speaker detail"
       
      elsif !params[:event_speaker][:photo]
        
           @event_speaker.update_attributes(params[:event_speaker])
         flash[:notice] = "Successfully Updated spekaer detail"        
      
       else
         flash[:error] = "Failed to update speaker"  
      end 
    
    
    end
    
    redirect_to event_url(params[:event_speaker][:event_id]) and return
    
  end

  def index
  end

  def show
  end

  def destroy
    if EventSpeaker.find(params[:id]).destroy
      flash[:notice] = "Successfully deleted"
      
    else
      flash[:error] = "Unable to delete"
    end
    redirect_to :back
      return false
  end
end
