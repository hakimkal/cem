class SessionsController < ApplicationController
  
  
  def new
    if signed_in?
      redirect_to dashboard_path
      return false
    end
  end
  
  def create  
  s=params[:session] if request.post?
  #@s=s
 @chk= check_username_or_email(s) if request.post?
  if @chk.nil?
   flash.now[:warning] = "Invalid Username or Password"
   render :new
   
 else
   
   sign_in @chk
   redirect_to dashboard_path
 end
  
 end
    
  
 def destroy
   session[:user] = nil
   flash.now[:notice] = "Logout Successful"
   sign_out
   redirect_to  root_path
 end
 
 
 private 
 
 def check_username_or_email (str={})
    
   email_format = /\A[\w+\-\d+.]+@[a-z\d+\-.]+\.[a-z]+\z/i
    if str[:username] =~ email_format
    # return str[:username]
      User.authenticate({:username=> str[:username],:password=> str[:password],:column=>'email'})
    else
       User.authenticate({:username => str[:username],:password => str[:password],:column=>'username'})
     #return str[:username]
     end
    
 end
end
