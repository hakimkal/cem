class EventsController < ApplicationController
  
  before_filter :login_required , :except=>[:index,:show]
  layout :user_layout
  before_filter :check_staff? , :except => [:show,:index]
  
  def new
    @event = Event.new
  end

  def edit
    @event = Event.find(params[:id],:include=>[:event_speakers,:event_images])
    
  end

  def create
    @event = Event.new(params[:event])
    if @event.save
    
    @event_id = @event.id
    es = params[:data][:event_speaker]
    es.delete_if{|f| f[:fullname].blank?}
    
    es.each do |s|
      s[:event_id] = @event_id
      s[:user_id] = current_user.id
      e_v_s = EventSpeaker.new s if !s[:fullname].nil?
      e_v_s.save
    end
    
    flash[:notice] = "Successfully saved new item"
    
    redirect_to events_url and return
    
    else
      flash[:error] = "Unable to save"
      render :new
    end
   
  end

  def update
    if request.put?
      
     @newspub = @model= Event.find(:first,:conditions=>{:id =>params[:id]})
     if @newspub.nil?
       flash[:error] =" Invalid Selection detected. "
       redirect_to  events_path
       return nil
     end
       #find(:first,:conditions=>{"user_id"=>current_user.id,"id"=>params[:newspub][:id]})
      
      if (params[:event][:logo])
              
       @newspub.logo=nil
       @newspub.save
       
      end      
        
       params[:event][:user_id] = current_user.id
       # @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
      
       if @newspub.update_attributes(params[:event])
       flash[:notice] = "Successfully Updated the Event: #{params[:event][:title]}"
       
       elsif !params[:event][:logo]
        
         params[:event][:user_id] = current_user.id
         # @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
         @newspub.update_attributes(params[:event])
         flash[:notice] = "Successfully Updated the Event  #{params[:event][:title]}"        
       else
         flash[:error] = "Failed to update the Event"  
      end
      
      redirect_to events_path
      end
  end

  def index
    
    @events = Event.paginate(:page=>params[:page],:per_page =>10)
  end

  def show
    if params[:id].blank? and request.host == "subcaster.local"
      params[:id] = '18'
    elsif params[:id].blank? and request.host != "subcaster.local"
      params[:id] = '2'
    
    end
    @event = Event.find(params[:id],:include =>[:event_images, :event_speakers])
  end

  def destroy
 
      @event = Event.find_by_id(params[:id])
      session[:e] = @event
      #@event.logo = nil
       
       if @event.destroy
       flash[:notice] = "Successfully deleted item"
      else
        flash[:error] = "unable to delete item"
      
    end
    redirect_to events_url and return
  end
end
