# == Schema Information
#
# Table name: participants
#
#  id                      :integer          not null, primary key
#  name                    :string(255)
#  title                   :string(255)
#  event_id                :integer
#  day_in                  :string(255)
#  date_in                 :date
#  day_out                 :string(255)
#  date_out                :date
#  ticket_cost             :string(255)
#  discounted              :string(255)
#  uni_id                  :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  firstname               :string(255)
#  lastname                :string(255)
#  ng_state_id             :integer
#  ng_lg_id                :integer
#  phone                   :string(255)
#  email                   :string(255)
#  address                 :text
#  office                  :text
#  photo_file_name         :string(255)
#  photo_content_type      :string(255)
#  photo_file_size         :integer
#  photo_updated_at        :datetime
#  othername               :string(255)
#  gender                  :string(255)
#  occupation              :string(255)
#  position_currently_held :string(255)
#  lgward                  :string(255)
#  closing_ceremony        :string(255)
#  event_recordings        :string(255)
#  attending_dinner        :string(255)
#  followup_info           :string(255)
#  payment_mode            :string(255)
#  phone2                  :string(255)
#

class Participant < ActiveRecord::Base
  belongs_to :event
  belongs_to :user
  belongs_to :ng_state
  attr_accessor :user_id 
  
  attr_accessible :phone2,:closing_ceremony, :lgward,:othername ,:gender,:occupation,:position_currently_held,:event_recordings,:attending_dinner,:followup_info,:payment_mode,:photo , :phone, :email,:address, :office, :date_in,:ng_state_id, :user_id,:ng_lg_id,:firstname,:lastname ,:date_out, :day_in, :day_out, :discounted, :event_id, :name, :ticket_cost, :title, :uni_id

  validates :uni_id , :presence => true,
                      :uniqueness => {:case_sensitive =>false}
  validates :event_id , :presence => true
  validates :ticket_cost , :presence => true
  
  has_attached_file  :photo ,:styles => { :medium => "200x160>", :thumb => "100x100>" },:url => "/uploads/participants_photos/:id/:style/:basename.:extension",
    :path =>":rails_root/public/uploads/participants_photos/:id/:style/:basename.:extension"
 
 before_save :check_record 
 
 
 private
 
 def check_record
   
   if new_record?
      ex  = Participant.count(:conditions =>{
        :event_id => event_id , 
        :firstname => firstname ,
        :lastname => lastname,
        :uni_id => uni_id,
        :ng_state_id => ng_state_id,
        #:email => email,
        :phone => phone,
        :phone2 => phone2,
       # :photo_file_name => photo_file_name 
         
        })
      return false if ex > 0 
    end
 
   
   
 end
end
