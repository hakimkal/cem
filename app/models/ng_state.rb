# == Schema Information
#
# Table name: ng_states
#
#  id         :integer          not null, primary key
#  state      :string(255)
#  capital    :string(255)
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NgState < ActiveRecord::Base
  has_one :participant
  attr_accessible :capital, :code, :state
  
  validates  :state,  :presence => true,
                          
                         :uniqueness => {:case_sensitive=>false}
end
