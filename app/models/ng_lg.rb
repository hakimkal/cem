# == Schema Information
#
# Table name: ng_lgs
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  state_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NgLg < ActiveRecord::Base
  attr_accessible :name, :state_id
end
