# == Schema Information
#
# Table name: events
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  start_date          :date
#  start_time          :time
#  end_time            :time
#  end_date            :date
#  location            :string(255)
#  about               :text
#  user_id             :integer
#  created_by          :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  logo_file_name      :string(255)
#  logo_content_type   :string(255)
#  logo_file_size      :integer
#  logo_updated_at     :datetime
#  participants_number :integer
#  tickteting          :text
#

class Event < ActiveRecord::Base
  
  attr_accessible :about, :logo,:tickteting, :participants_number,:created_by, :end_date, :end_time, :location, :name, :start_date, :start_time, :user_id
  has_many :event_images
  has_many :event_speakers
  has_many :event_partners
  has_many :participants
  has_attached_file :logo ,:styles => { :medium => "300x300>", :thumb => "200x160>" },:url => "/uploads/events_logo/:id/:style/:basename.:extension",
    :path =>":rails_root/public/uploads/events_logo/:id/:style/:basename.:extension"
 
 validates  :name,  :presence => true,
                          
                         :uniqueness => {:case_sensitive=>false}
                         
 validates :user_id , :presence => true
  
end
