# == Schema Information
#
# Table name: finances
#
#  id                  :integer          not null, primary key
#  event_id            :integer
#  price               :string(255)
#  discount_percentage :string(255)
#  custom_discount     :string(255)
#  user_id             :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Finance < ActiveRecord::Base
  attr_accessible :custom_discount, :discount_percentage, :event_id, :price, :user_id
end
