# == Schema Information
#
# Table name: event_partners
#
#  id                :integer          not null, primary key
#  event_id          :integer
#  name              :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  logo_file_name    :string(255)
#  logo_content_type :string(255)
#  logo_file_size    :integer
#  logo_updated_at   :datetime
#

class EventPartner < ActiveRecord::Base
  attr_accessible :event_id, :name , :logo
  belongs_to :event
  has_attached_file :logo ,:styles => { :medium => "100x100>", :thumb => "100x80>" },:url => "/uploads/events_partners/:id/:style/:basename.:extension",
    :path =>":rails_root/public/uploads/events_partners/:id/:style/:basename.:extension"
 
 validates  :name,  :presence => true
                         
 validates :event_id , :presence => true

  before_save :check_record
  
  def check_record
    
    if new_record?
      ex  = EventPartner.count(:conditions =>{
        :event_id => event_id , 
        :name => name ,
        :logo_file_name => logo_file_name ,
         
        })
      return false if ex > 0 
    end
 
  end
end
