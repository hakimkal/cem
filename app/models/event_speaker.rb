# == Schema Information
#
# Table name: event_speakers
#
#  id                 :integer          not null, primary key
#  fullname           :string(255)
#  bio                :text
#  event_id           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  url                :string(255)
#

class EventSpeaker < ActiveRecord::Base
 
  attr_accessible :bio, :event_id, :url, :fullname,:photo
   belongs_to :event
   
    has_attached_file :photo ,:styles => { :medium => "500x300>", :thumb => "200x160>" },:url => "/uploads/speakers/:id/:style/:basename.:extension",
                              :path =>":rails_root/public/uploads/speakers/:id/:style/:basename.:extension"
   

   validates :event_id , :presence => true
 
end
