# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  email              :string(255)
#  salt               :string(255)
#  firstname          :string(255)
#  lastname           :string(255)
#  suspend            :string(255)
#  group_id           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  username           :string(255)
#  encrypted_password :string(255)
#

class User < ActiveRecord::Base
  
  belongs_to :group
  attr_accessor :password , :password_confirmation
  attr_accessible :email, :password, :password_confirmation,:firstname, :group_id, :lastname, :salt, :suspend,:username
  
  
  email_format = /\A[\w+\-\d.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # password_format = /\A.*(?=[a-z])(?=.*[A-Z])(?=.*[0-9])(.*[\w_]).*\z/
 # password_format = /\A.*(?=[a-z]|[A-Z])(?=.*[A-Z])(?=.*[0-9_@\$]).*\z/
  validates  :username,  :presence => true,
                         :length => {:maximum => 100},
                         :uniqueness => {:case_sensitive=>false} #,
                         #:on => :create
                         
  
  validates  :firstname,  :presence => true,
                         :length => {:maximum => 100}
                           
 # validates  :lastname,  :presence => true,
               #          :length => {:maximum => 100}
  #                       
  validates  :group_id,  :presence => true
                                                                            
  validates   :email ,   :presence => true,
                         :format => {:with => email_format },
                         :uniqueness => {:case_sensitive=>false}
                         
  #:within => 8..40
  validates  :password , :presence => true,
                         :confirmation =>true,
                         :length => {:within => 6..40}, #minimum=>"",:maximum=>""
                         #:format => {:with => password_format ,
                        # :message=> "must contain at least 1 lowercase letter, 1 uppercase letter, 1 digit."},
                         :if => Proc.new { |u| !u.password.blank? || !u.password.nil? }
                         #:if => Proc.new { |user| !user.password.blank? || !user.password_confirmation.blank? || user.new_record?}

                         
                         
   
   
  before_save :encrypt_password,:if => Proc.new { |user| !user.password.blank? || !user.password_confirmation.blank? || user.new_record?}



  def has_password? submitted_password
    
    encrypted_password == encrypt(submitted_password)
    
  end
  
  def self.authenticate(s={})
       
     if s[:column] == 'email'
    @user = find_by_email(s[:username])
    else
    @user = find_by_username(s[:username])
    end
    
    if @user.nil?
    
    return nil
    elsif  @user.has_password?(s[:password])
        return @user
    else
      return nil
    end
  end
  
  def self.authenticate_with_salt(id,cookie_salt)
    
    if id.nil? 
      user = nil
      
    else
    user = find_by_id(id)
    (user && user.salt == cookie_salt) ? user : nil
  end
 end
   
    
  def custom_update(model={})
    model.delete :password
    model.delete :password_confirmation
     update_attributes! model
     
  end
  
  
  private
  
  
  
 def make_salt  
     
    self.salt = Digest::SHA2.hexdigest("#{Time.now.utc}--#{password}")
  end
  
  def encrypt_password 
    self.salt = make_salt if new_record?
    self.encrypted_password = encrypt(password)
  end
  
  def encrypt str
   security_hash str
  end
  
  def security_hash str
    Digest::SHA2.hexdigest("#{self.salt}" + str)
  end
end
