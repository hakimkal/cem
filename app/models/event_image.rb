# == Schema Information
#
# Table name: event_images
#
#  id                 :integer          not null, primary key
#  event_id           :integer
#  user_id            :integer
#  created_by         :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

class EventImage < ActiveRecord::Base
  attr_accessible :created_by, :event_id, :user_id,:image
  
  belongs_to :events
  belongs_to :user
  
  
  
  has_attached_file :image ,:styles => { :medium => "300x300>", :thumb => "200x160>" },:url => "/uploads/events_logo/:id/:style/:basename.:extension",
    :path =>":rails_root/public/uploads/events_logo/:id/:style/:basename.:extension"

end
