class AddMoreFieldsToParticipants < ActiveRecord::Migration
  def change
    add_column :participants, :othername, :string
    add_column :participants, :gender, :string
    add_column :participants, :occupation, :string
    add_column :participants, :position_currently_held, :string
    add_column :participants, :lgward, :string
    add_column :participants, :closing_ceremony, :string
    add_column :participants, :event_recordings, :string
    add_column :participants, :attending_dinner, :string
    add_column :participants, :followup_info, :string
    add_column :participants, :payment_mode, :string
  end
end
