class CreateEventImages < ActiveRecord::Migration
  def change
    create_table :event_images do |t|
      t.integer :event_id
      t.integer :user_id
      t.string :created_by

      t.timestamps
    end
  end
end
