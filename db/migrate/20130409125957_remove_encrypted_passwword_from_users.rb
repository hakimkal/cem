class RemoveEncryptedPasswwordFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :encrypted_passwword
  end

  def down
    add_column :users,  :encrypted_passwword
  end
end
