class AddPhone2ToParticipants < ActiveRecord::Migration
  def change
    add_column :participants, :phone2, :string
  end
end
