class AddAttachmentLogoToEventPartners < ActiveRecord::Migration
  def self.up
    change_table :event_partners do |t|
      t.attachment :logo
    end
  end

  def self.down
    drop_attached_file :event_partners, :logo
  end
end
