class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.string :name
      t.string :title
      t.integer :event_id
      t.string :day_in
      t.date :date_in
      t.string :day_out
      t.date :date_out
      t.string :ticket_cost
      t.string :discounted
      t.string :uni_id

      t.timestamps
    end
  end
end
