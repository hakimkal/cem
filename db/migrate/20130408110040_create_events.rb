class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.date :start_date
      t.time :start_time
      t.time :end_time
      t.date :end_date
      t.string :location
      t.text :about
      t.integer :user_id
      t.string :created_by

      t.timestamps
    end
  end
end
