class AddAttachmentPhotoToEventSpeakers < ActiveRecord::Migration
  def self.up
    change_table :event_speakers do |t|
      t.attachment :photo
    end
  end

  def self.down
    drop_attached_file :event_speakers, :photo
  end
end
