class AddParticipantsNumberToEvents < ActiveRecord::Migration
  def change
    add_column :events, :participants_number, :integer
  end
end
