class AddStatesToParticipants < ActiveRecord::Migration
  def change
    add_column :participants, :ng_state_id, :integer
    add_column :participants, :ng_lg_id, :integer
  end
end
