class AddFnameLnameToParticipants < ActiveRecord::Migration
  def change
    add_column :participants, :firstname, :string
    add_column :participants, :lastname, :string
  end
end
