class AddFieldsToParticipants < ActiveRecord::Migration
  def change
    add_column :participants, :phone, :string
    add_column :participants, :email, :string
    add_column :participants, :address, :text
    add_column :participants, :office, :text
  end
end
