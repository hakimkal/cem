class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :encrypted_passwword
      t.string :salt
      t.string :firstname
      t.string :lastname
      t.string :suspend
      t.integer :group_id

      t.timestamps
    end
  end
end
