class CreateEventSpeakers < ActiveRecord::Migration
  def change
    create_table :event_speakers do |t|
      t.string :fullname
      t.text :bio
      t.integer :event_id

      t.timestamps
    end
  end
end
