class CreateNgLgs < ActiveRecord::Migration
  def change
    create_table :ng_lgs do |t|
      t.string :name
      t.integer :state_id

      t.timestamps
    end
  end
end
