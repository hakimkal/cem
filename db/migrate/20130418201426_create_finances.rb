class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.integer :event_id
      t.string :price
      t.string :discount_percentage
      t.string :custom_discount
      t.integer :user_id

      t.timestamps
    end
  end
end
