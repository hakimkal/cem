class AddUrllinkToEventSpeakers < ActiveRecord::Migration
  def change
    add_column :event_speakers, :url, :string
  end
end
