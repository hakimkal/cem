class CreateEventPartners < ActiveRecord::Migration
  def change
    create_table :event_partners do |t|
      t.integer :event_id
      t.string :name

      t.timestamps
    end
  end
end
