class AddAttachmentPhotoToParticipants < ActiveRecord::Migration
  def self.up
    change_table :participants do |t|
      t.attachment :photo
    end
  end

  def self.down
    drop_attached_file :participants, :photo
  end
end
