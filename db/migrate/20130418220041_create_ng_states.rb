class CreateNgStates < ActiveRecord::Migration
  def change
    create_table :ng_states do |t|
      t.string :state
      t.string :capital
      t.string :code

      t.timestamps
    end
  end
end
