# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130508190908) do

  create_table "event_images", :force => true do |t|
    t.integer  "event_id"
    t.integer  "user_id"
    t.string   "created_by"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "event_partners", :force => true do |t|
    t.integer  "event_id"
    t.string   "name"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  create_table "event_speakers", :force => true do |t|
    t.string   "fullname"
    t.text     "bio"
    t.integer  "event_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "url"
  end

  create_table "events", :force => true do |t|
    t.string   "name"
    t.date     "start_date"
    t.time     "start_time"
    t.time     "end_time"
    t.date     "end_date"
    t.string   "location"
    t.text     "about"
    t.integer  "user_id"
    t.string   "created_by"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.integer  "participants_number"
    t.text     "tickteting"
  end

  create_table "finances", :force => true do |t|
    t.integer  "event_id"
    t.string   "price"
    t.string   "discount_percentage"
    t.string   "custom_discount"
    t.integer  "user_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "ng_lgs", :force => true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "ng_states", :force => true do |t|
    t.string   "state"
    t.string   "capital"
    t.string   "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "participants", :force => true do |t|
    t.string   "name"
    t.string   "title"
    t.integer  "event_id"
    t.string   "day_in"
    t.date     "date_in"
    t.string   "day_out"
    t.date     "date_out"
    t.string   "ticket_cost"
    t.string   "discounted"
    t.string   "uni_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "firstname"
    t.string   "lastname"
    t.integer  "ng_state_id"
    t.integer  "ng_lg_id"
    t.string   "phone"
    t.string   "email"
    t.text     "address"
    t.text     "office"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "othername"
    t.string   "gender"
    t.string   "occupation"
    t.string   "position_currently_held"
    t.string   "lgward"
    t.string   "closing_ceremony"
    t.string   "event_recordings"
    t.string   "attending_dinner"
    t.string   "followup_info"
    t.string   "payment_mode"
    t.string   "phone2"
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "salt"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "suspend"
    t.integer  "group_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "username"
    t.string   "encrypted_password"
  end

end
