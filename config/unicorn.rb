working_directory "/home/deploy/cem"
pid "/home/deploy/tmp/pids/unicorn.pid"
stderr_path "/home/deploy/cem/log/unicorn.log"
stdout_path "/home/deploy/cem/log/unicorn.log"
listen "/tmp/unicorn.cem.sock"
worker_processes 3
timeout 30