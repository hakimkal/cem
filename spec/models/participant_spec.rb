# == Schema Information
#
# Table name: participants
#
#  id                      :integer          not null, primary key
#  name                    :string(255)
#  title                   :string(255)
#  event_id                :integer
#  day_in                  :string(255)
#  date_in                 :date
#  day_out                 :string(255)
#  date_out                :date
#  ticket_cost             :string(255)
#  discounted              :string(255)
#  uni_id                  :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  firstname               :string(255)
#  lastname                :string(255)
#  ng_state_id             :integer
#  ng_lg_id                :integer
#  phone                   :string(255)
#  email                   :string(255)
#  address                 :text
#  office                  :text
#  photo_file_name         :string(255)
#  photo_content_type      :string(255)
#  photo_file_size         :integer
#  photo_updated_at        :datetime
#  othername               :string(255)
#  gender                  :string(255)
#  occupation              :string(255)
#  position_currently_held :string(255)
#  lgward                  :string(255)
#  closing_ceremony        :string(255)
#  event_recordings        :string(255)
#  attending_dinner        :string(255)
#  followup_info           :string(255)
#  payment_mode            :string(255)
#  phone2                  :string(255)
#

require 'spec_helper'

describe Participant do
  pending "add some examples to (or delete) #{__FILE__}"
end
